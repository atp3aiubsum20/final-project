<?php

use App\Http\Middleware\CheckAdmin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', ['uses'=>'RootController@index']);
Auth::routes();

//github

Route::get('login/github', 'Auth\LoginController@redirectToProvider');
Route::get('login/github/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/', function (){return redirect('/login');});

Route::post('/post', ['uses'=>'postController@post']);

//Message
Route::get('/message',['uses'=>'messageController@default']);
Route::get('/message/{recid}',['uses'=>'messageController@index']);
Route::post('/message/{recid}',['uses'=>'messageController@sendMessage']);


Route::get('/home', 'HomeController@index')->name('home');

#user
Route::get('/user/addpersonaldetails',['uses'=>'UserControllers@addPersonalInfo']);
Route::post('/user/addpersonaldetails',['uses'=>'UserControllers@storeInfo']);

Route::get('/user/{username}',['uses'=>'UserControllers@UserInfo']);

Route::get('/user/{username}/edit',['uses'=>'UserControllers@EditUserInfo']);
Route::post('/user/{username}/edit',['uses'=>'UserControllers@UpdateUserInfo']);

Route::get('/usersearch',['uses'=>'UserControllers@searchUser']);

Route::post('/user/downloadData',['uses'=>'UserControllers@toPDF']);

#post
Route::get('/post/{id}',['uses'=>'postController@showSinglePost']);
Route::get('/user/{username}/posts',['uses'=>'postController@showUserPosts']);

Route::get('/createpost',['uses'=>'postController@post']);
Route::post('/createpost',['uses'=>'postController@createPost']);

Route::get('/post/{id}/edit',['uses'=>'postController@editPostView']);
Route::post('/post/{id}/edit',['uses'=>'postController@editPost']);

Route::post('/post/{id}', ['uses'=>'postController@comment']);

//Admin
Route::middleware('checkadmin')->group(function(){
    Route::get('/admin', function (){return redirect('/admin/reports');})->name('admin');
    Route::get('/admin/reports', ['uses' => 'adminController@reportsIndex']);

    Route::get('/admin/announcements', ['uses' => 'adminController@announcementsIndex']);
    Route::post('/admin/announcements', ['uses' => 'adminController@announcementsSend']);

    Route::get('/admin/members', ['uses' => 'adminController@membersIndex']);
    Route::post('/admin/banAction', ['uses' => 'adminController@banAction']);

    Route::get('/admin/status', ['uses' => 'adminController@statusIndex']);
    Route::post('/admin/status/toPDF', ['uses' => 'adminController@toPDF']);
});


//Friends
Route::get('/friends', ['uses' => 'friendController@index']);
Route::post('/friends/fReqAction', ['uses' => 'friendController@fReqAction']);