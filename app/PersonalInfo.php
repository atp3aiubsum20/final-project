<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class personalInfo extends Model
{
    protected $table = 'personal_infos';
    protected $fillable = [
        'userid',
        'name',
        'email',
        'username',
        'dob',
        'school',
        'displayimg',
        'college',
        'university'
    ];
}
