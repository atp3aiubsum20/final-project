<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class postReq extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'=>'required',
            'postimg'=>'image|nullable|mimes:jpeg,jgp,png,gif'
        ];
    }
    public function messages()
    {
        return [
            'description.required' => 'Description needed',
            'postimg.mimes'=>'Supported Type :jpeg,jpg,png,gif',
            'postimg.image'=>'File incorrect format: not image'
        ];
    }
}
