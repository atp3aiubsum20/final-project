<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'dob'=> 'required',
            'propic'=>'image|nullable|mimes:jpeg,jgp,png,gif'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Please enter your Name',
            'name.max' => 'Name cannot exceed 255',
            'dob.required' => 'Please enter your DOB',
            'propic.mimes'=>'Supported Type :jpeg,jpg,png,gif',
            'propic.image'=>'File incorrect format: not image'
        ];
    }
}
