<?php

namespace App\Http\Controllers;

use App\Friend;
use App\Message;
use App\Events\MessageSent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class messageController extends Controller
{
    function default(){
        $id = Auth::user()->id;
        $latest = Message::where('senderid', $id)->orWhere('receiverid', $id)->orderBy('time', 'DESC')->first();
        if($latest){
            if($latest->senderid == $id){
                $latestid = $latest->receiverid;
            } elseif ($latest->receiverid == $id){
                $latestid = $latest->senderid;
            }
            return redirect('/message/' . $latestid);
        } else {
            return redirect('/message/0');
        }
    }
    
    function index($recid, Request $request){
        $id = Auth::user()->id;

        Message::where('senderid', $recid)->where('receiverid', $id)
                ->update(['seen'=>'1']);

        $messages = Message::where(function($query) use ($id, $recid) {
                                $query->where('senderid', $id)->where('receiverid', $recid);})
                            ->orWhere(function($query) use ($id, $recid) {
                                $query->where('senderid', $recid)->where('receiverid', $id);})
                            ->orderBy('time', 'ASC')->get();
        $user = ['from' => $id, 'to' => $recid];

        $friends = DB::table('friends')
                    ->where('personid', $id)
                    ->where('accept', '1')
                    ->rightJoin('personal_infos', 'personal_infos.user_id', '=', 'friends.friendid')
                    ->get();

        foreach ($friends as $friend) {
            $lastmsg = Message::where(function($query) use ($id, $friend) {
                                $query->where('senderid', $id)->where('receiverid', $friend->friendid);})
                            ->orWhere(function($query) use ($id, $friend) {
                                $query->where('senderid', $friend->friendid)->where('receiverid', $id);})
                            ->orderBy('time', 'DESC')->first();
            $friend->lastmsg = $lastmsg;
            
            $unseen = Message::where('senderid', $recid)->where('receiverid', $id)->where('seen', '0')
                            ->orderBy('time', 'DESC')->get()->count();
            $friend->unseen = $unseen;
        }
        
        return view('message.index')->with('user', $user)->with('messages', $messages)->with('friends', $friends);
    }

    function sendMessage($recid, Request $request){
        if ($recid == '0') {
            return response('Invalid receiver!');
        }
        $message = new Message;
        $message->senderid = Auth::user()->id;
        $message->receiverid = $request->receiverid;
        $message->description = $request->description;
        $message->save();

        broadcast(new MessageSent($message));
    }
}
