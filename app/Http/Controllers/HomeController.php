<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Post;
use App\friends;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        if (Auth::user()->type=='admin') {
            return redirect()->route('admin');
        }
        else if(Auth::user()->type=='member'){
            if(Auth::user()->flag=="new"){
                return redirect('user/addpersonaldetails');
            }
            else{
                /* $posts = Post::where('posterid', $id)->get();
                $postids=[];
                foreach ($posts as $post) {
                    array_push($postids, $post->id);
                }
                return view('post.userPosts')->with('postids', json_encode($postids)); */
                return view('home');
            }
        }
    }
}
