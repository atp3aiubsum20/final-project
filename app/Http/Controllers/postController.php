<?php

namespace App\Http\Controllers;

use App\comment;
use Illuminate\Http\Request;
use App\Http\Requests\PostReq;
use App\Http\Requests\editPostRequest;
use App\Post;
use App\User;
use Carbon\Carbon;
use App\Friend;
use Auth;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class postController extends Controller
{
    function post(){
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (Auth::user()->type=='admin') {
            return redirect()->route('admin');
        }
        else if(Auth::user()->type=='member'){
            if(Auth::user()->flag=="new"){
                return redirect('/user/addpersonaldetails');
            }
            else{
                return view('post.createpost');
            }
        }
    }
    function createPost(postReq $request){

        if($request->hasFile('postimg')){
            $filenameWithExt=$request->file('postimg')->getClientOriginalName();
            $filename= pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('postimg')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path = $request->file('postimg')->storeAs('public/PostImages',$fileNameToStore);
            $post = new Post();
            $post->posterid = Auth::user()->id;
            $post->description = $request->description;
            $post->postimg = $fileNameToStore;
            $post->time= Carbon::now();
            $post->save();
            return redirect('/post/'.$post->id);
        }
        else{
            $post = new Post();
            $post->posterid = Auth::user()->id;
            $post->description = $request->description;
            $post->postimg = null;
            $post->time= Carbon::now();
            $post->save();
            return redirect('/post/'.$post->id);
        }
    }
    
    function showSinglePost($id){
        
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $post = Post::find($id);
        $poster = User::find($post->posterid);

        $post->comments = comment::where('post', $id)
                            ->get();

        foreach ($post->comments as $comment) {
            $username = User::where('id', $comment->posterid)
                            ->first()->username;
            $comment->commenter = $username;
        }


        $temp= Friend::where('personid',Auth::user()->id)->where('friendid',$poster->id)->where('accept','1')->first();
        if((($temp || $poster->id==Auth::user()->id) && !(Auth::user()->flag=="new")) || Auth::user()->type=="admin" ){
            return view('Post.post')->with('post',$post)->with('poster',$poster);
        }
        else{
            return redirect('/home');
        }
    }
    function editPostView($id){
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $post = Post::find($id);
        if($post->posterid==Auth::user()->id){
            return view('post.editpost')->with('post',$post);
        }
        else{
            return redirect('/post/'.$id);
        }
    }
    function editPost(editPostRequest $request,$id){
        $post = Post::find($id);
        $post->description=$request->description;
        $post->save();
        return redirect('/post/'.$id);
    }

    function showUserPosts($username){
        
        $id = User::where('username', $username)->first()->id;
        $posts = Post::where('posterid', $id)->get();

        $postids=[];
        foreach ($posts as $post) {
            array_push($postids, $post->id);
        }

        return view('post.userPosts')->with('postids', json_encode($postids));
    }

    function comment($id, Request $request){
        $comment = new comment;
        $comment->posterid = Auth::user()->id;
        $comment->post = $id;
        $comment->description = $request->description;
        $comment->save();

        return redirect('/post/'.$id);
    }
}
