<?php

namespace App\Http\Controllers;

use App\Friend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class friendController extends Controller
{
    function index(){
        $frndReqs = Friend::where('friendid', Auth::user()->id)
                        ->where('accept','0')
                        ->join('personal_infos','personal_infos.user_id','personid')
                        ->get();

        $frndList = Friend::where('personid', Auth::user()->id)
                        ->where('accept','1')
                        ->join('personal_infos','personal_infos.user_id','friendid')
                        ->get();
        return view('friends.index')->with('frndReqs', $frndReqs)->with('frndList', $frndList);
    }

    function fReqAction(Request $request){
        if($request->action == 'accept'){
            $accept1 = Friend::where('personid', $request->reqId)
                            ->where('friendid', Auth::user()->id)
                            ->first();
            $accept1->accept = '1';
            $accept1->save();

            $accept2 = new Friend;
            $accept2->friendid = $request->reqId;
            $accept2->personid = Auth::user()->id;
            $accept2->accept = '1';
            $accept2->save();
        }

        else if($request->action == 'reject'){
            $reject = Friend::where('personid', $request->reqId)
                            ->where('friendid', Auth::user()->id)
                            ->first();
            $reject->delete();
        }

        else if($request->action == 'add'){
            $add = new Friend;
            $add->friendid = $request->reqId;
            $add->personid = Auth::user()->id;
            $add->save();
        }

        else if($request->action == 'cancel'){
            $cancel = Friend::where('personid', Auth::user()->id)
                            ->where('friendid', $request->reqId)
                            ->first();
            $cancel->delete();
        }

        else if($request->action == 'unfriend'){
            $unfriend2 = Friend::where('personid', Auth::user()->id)
                            ->where('friendid', $request->reqId)
                            ->first();
            $unfriend2->delete();
            
            $unfriend1 = Friend::where('personid', $request->reqId)
                            ->where('friendid', Auth::user()->id)
                            ->first();
            $unfriend1->delete();
        }

        return redirect('/friends');
    }
}