<?php

namespace App\Http\Controllers;

use App\Announcement;
use App\Events\AnnouncementSent;
use App\User;
use Carbon\Traits\Date;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class adminController extends Controller
{
    //Reports 
    function reportsIndex(){

        $reports = DB::table('reports')
                    ->join('post', 'post.id', '=', 'reports.postid')
                    ->join('personal_infos AS reporter', 'reporter.id', '=', 'reports.reporterid')
                    ->join('personal_infos AS poster', 'poster.id', '=', 'post.posterid')
                    
                    ->select('reports.id AS id')
                    ->addSelect('reports.time AS time')
                    ->addSelect('reports.type AS type')
                    ->addSelect('reports.reason AS reason')

                    ->addSelect('post.id AS postid')

                    ->addSelect('poster.username AS poster')
                    ->addSelect('poster.id AS posterid')

                    ->addSelect('reporter.username AS reporter')
                    ->addSelect('reporter.id AS reporterid')

                    ->get();

        return view('admin.reports')->with('reports', $reports);
    }

    function announcementsIndex(){
        $announcements = DB::table('announcements')
                    ->join('personal_infos AS sender', 'sender.id', '=', 'announcements.senderid')
                    
                    ->select('announcements.id AS id')
                    ->addSelect('sender.username AS sender')
                    ->addSelect('sender.id AS senderid')
                    ->addSelect('announcements.message AS message')
                    ->addSelect('announcements.time AS time')

                    ->get();

        return view('admin.announcements')->with('announcements', $announcements);
    }

    function announcementsSend(Request $request){
        $announcement = new Announcement;
        $announcement->senderid = Auth::user()->id;
        $announcement->message = $request->message;
        $announcement->save();

        broadcast(new AnnouncementSent($announcement));

    }

    function membersIndex(Request $request){

        if($request->query('search') && $request->query('search') != ''){
            
            $search = $request->query('search');
            $members = DB::table('personal_infos')
                    ->join('users AS login', 'login.id', '=', 'personal_infos.user_id')
                    ->where(function ($q) use ($search) {
                        return $q->where('personal_infos.name','LIKE','%'. $search .'%')->orWhere('personal_infos.username','LIKE','%'. $search .'%');
                    })
                    ->where('login.type','member')
                    
                    ->select('personal_infos.id AS id')
                    ->addSelect('personal_infos.username AS username')
                    ->addSelect('personal_infos.name AS fname')
                    ->addSelect('personal_infos.email AS email')
                    ->addSelect('personal_infos.dob AS dob')
                    ->addSelect('personal_infos.joiningdate AS joiningdate')
                    ->addSelect('login.ban AS ban')

                    ->get();
        } else {
            $members = DB::table('personal_infos')
                    ->join('users AS login', 'login.id', '=', 'personal_infos.user_id')
                    ->where('login.type','member')

                    ->select('personal_infos.id AS id')
                    ->addSelect('personal_infos.username AS username')
                    ->addSelect('personal_infos.name AS fname')
                    ->addSelect('personal_infos.email AS email')
                    ->addSelect('personal_infos.dob AS dob')
                    ->addSelect('personal_infos.joiningdate AS joiningdate')
                    ->addSelect('login.ban AS ban')

                    ->get();
        }
        return view('admin.members')->with('members', $members);
    }

    function banAction(Request $request){
        if(Auth::user()->type == 'admin'){
            if($request->action == 'ban'){
                $target = User::find($request->target);
                $target->ban = '1';
                $target->save();
            } elseif ($request->action == 'unban'){
                $target = User::find($request->target);
                $target->ban = '0';
                $target->save();
            }
        } else {
            return response('Forbidden', 403);
        }
    }

    function statusIndex(){
        $userdata = DB::table('users')->join('personal_infos', 'users.id', 'personal_infos.user_id')
                        ->where('users.flag','old')->get();
        return view('admin.status')->with('userdata', $userdata);
    }

    function toPDF(){
        $userdata = DB::table('users')->join('personal_infos', 'users.id', 'personal_infos.user_id')
                        ->where('users.flag','old')->get();

        $pdf = PDF::loadView('admin.pdf', compact('userdata'));
        return $pdf->download('adminReport'.date('ymd_His').'.pdf');
    }
}
