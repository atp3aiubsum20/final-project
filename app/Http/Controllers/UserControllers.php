<?php

namespace App\Http\Controllers;

use App\Friend;
use Illuminate\Http\Request;
use App\Http\Requests\PersonalInfoRequest;
use App\Http\Requests\editPostRequest;
use App\Http\Requests\UserInfoEdit;
use Auth;
use App\personalInfo;
use App\User;
use Illuminate\Support\Facades\DB;
use PDF;

class UserControllers extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function addPersonalInfo(){
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (Auth::user()->type=='admin') {
            return redirect()->route('admin');
        }
        else if(Auth::user()->type=='member'){
            if(Auth::user()->flag=="new"){
                return view('Users.addPersonalInfo');
            }
            else{
                return redirect('user/personaldetails');
            }
        }
    }
    public function storeInfo(PersonalInfoRequest $request){

        if($request->hasFile('propic')){
            $filenameWithExt=$request->file('propic')->getClientOriginalName();
            $filename= pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('propic')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path = $request->file('propic')->storeAs('public/Propics',$fileNameToStore);
        }
        else{
            $fileNameToStore="noimage.png";
        }
        
        $info = new personalInfo();
        $info->user_id=Auth::user()->id;
        $info->name=$request->name;
        $info->email=Auth::user()->email;
        $info->username=Auth::user()->username;
        $info->dob=$request->dob;
        $info->school=$request->school;
        $info->college=$request->college;
        $info->university=$request->university;
        $info->displayimg=$fileNameToStore;
        $info->save();

        $user = User::find(Auth::user()->id);
        $user->flag = "old";
        $user->save();
        Auth::user()->flag="old";
        $request->session()->put('username', Auth::user()->username );
        return redirect('home');
    }
    public function UserInfo($username){
        $info = personalInfo::where('username',$username)->first();

        //Friend actions
        $user = User::where('username', $username)->first();
        
        $friend = Friend::where('personid', Auth::user()->id)
                        ->where('friendid', $user->id)
                        ->first();

        $friend1 = Friend::where('personid', $user->id)
                        ->where('friendid', Auth::user()->id)
                        ->first();
        
        $friendAction ='';

        
        if($friend){
            
            if($friend->accept == '0'){
                $friendAction = 'cancel';
            } else if($friend->accept == '1'){
                $friendAction = 'unfriend';
            }

        } elseif($friend1) {
            $friendAction = 'accept';
        } else {
            $friendAction = 'add';
        }
        
        return view ('Users.userprofile')->with('info',$info)->with('friendAction', $friendAction)->with('user',$user);
    }
    public function searchUser(Request $request){
        $search =$request->query('search');
        $members = DB::table('personal_infos')
                    ->join('users AS login', 'login.id', '=', 'personal_infos.user_id')
                    ->where(function ($q) use ($search) {
                        return $q->where('personal_infos.name','LIKE','%'. $search .'%')->orWhere('personal_infos.username','LIKE','%'. $search .'%');
                    })
                    ->where('login.type','member')->get();
        return view('Users.search')->with('members',$members);
    }

    public function EditUserInfo($username){

        if (!Auth::check()) {
            return redirect()->route('login');
        }
        if (Auth::user()->type=='admin') {
            return redirect()->route('admin');
        }
        else if(Auth::user()->type=='member'){
            if(Auth::user()->flag=="new"){
                return view('Users.addPersonalInfo');
            }
            else{
                if(Auth::user()->username==$username){
                    $info = personalInfo::where('username',$username)->first();
                    return view ('Users.editprofile')->with('info',$info);
                }
                else{
                    return redirect('/user/'.$username);
                }
            }
        }
    }
    
    public function UpdateUserInfo(UserInfoEdit $request){
        //dd($request);
        if($request->hasFile('propic')){
            $filenameWithExt=$request->file('propic')->getClientOriginalName();
            $filename= pathinfo($filenameWithExt,PATHINFO_FILENAME);
            $extension=$request->file('propic')->getClientOriginalExtension();
            $fileNameToStore=$filename.'_'.time().'.'.$extension;
            $path = $request->file('propic')->storeAs('public/Propics',$fileNameToStore);

            $info1 = personalInfo::where('user_id',Auth::user()->id)->first();
            $info1->name=$request->name;
            $info1->dob=$request->dob;
            $info1->school=$request->school;
            $info1->college=$request->college;
            $info1->university=$request->university;
            $info1->displayimg=$fileNameToStore;
            $info1->save();
        }
        else{
            $info1 = personalInfo::where('user_id',Auth::user()->id)->first();
            $info1->name=$request->name;
            $info1->dob=$request->dob;
            $info1->school=$request->school;
            $info1->college=$request->college;
            $info1->university=$request->university;
            $info1->save();
        }
        return redirect('/user/'.Auth::user()->username);
    }
    public function toPDF(){
        $info = DB::table('users')->join('personal_infos', 'users.id', 'personal_infos.user_id')
                        ->where('users.flag','old')->where('users.id',Auth::user()->id)->first();

        //view()->share('status',$userdata);
        $pdf = PDF::loadView('Users.userinfopdf', compact('info'));
        return $pdf->download(Auth::user()->username.'-profileinfo'.date('ymd_His').'.pdf');
        //return view('admin.status')->with('userdata', $userdata);
    }

}

