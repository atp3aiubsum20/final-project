<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginReq;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use App\personalInfo;
use Auth;
use Hash;
use Socialite;
use Carbon\Carbon;


class LoginController extends Controller
{
   
    use AuthenticatesUsers;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(LoginReq $request)
    {   
        $input = $request->all();
  
        $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if(auth()->attempt(array($fieldType => $input['username'], 'password' => $input['password'], 'ban' => '0')))
        {   
            $find1 = strpos($input['username'], '@');
            $find2 = strpos($input['username'], '.');
            if(($find1 !== false && $find2 !== false && $find2 > $find1)){
                $user = User::where('email',$input['username'])->first();
                $request->session()->put('username', $user->username);
                return redirect()->route('home'); 
            }
            else{
                $request->session()->put('username', $input['username']);
                return redirect()->route('home');
            }
            
        }else{
            return view('auth.login')
                ->with('error','Email-Address And Password Are Wrong.');
        }
          
    }
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('github')->user();
        $user1 = new User();
        $user1->username= $user->getNickName();
        $user1->email= $user->getEmail();
        $user1->password= Hash::make($user->getName());
        $user1->flag= "old";
        $user1->save();

        $info = new personalInfo();
        $info->user_id=$user1->id;
        $info->name=$user->getName();
        $info->email=$user1->email;
        $info->username=$user1->username;
        $info->dob=Carbon::now();
        $info->save();

        return redirect('/user/'.$user1->username);

    }
}

