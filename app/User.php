<?php

namespace App;

class User extends \Illuminate\Foundation\Auth\User
{   
    protected $table = 'users';
    protected $fillable = ['username','email','password'];
}
