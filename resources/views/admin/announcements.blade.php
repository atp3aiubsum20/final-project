<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
</head>

<style>
    #tblAnnouncements td{
        text-align: center;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}',
        }
    });
    
    $('#btnAnnounce').click(function(e){
        var data={
            'message': $('#inpAnnouncement').val(),
        }

        $.ajax({
            type: "post",
            data: data,
        });
    });

    $('#inpAnnouncement').on('keypress',function(e) {
        if(e.which == 13) {
            $('#btnAnnounce').trigger('click');
            $('#inpAnnouncement').val('');
        }
    });

    // Pusher
    var pusher = new Pusher('e2d10f9ae5c8245db0b4', {
      cluster: 'ap1'
    });

    var channel = pusher.subscribe('announcement');
    channel.bind('App\\Events\\AnnouncementSent', function(data) {
        $.ajax({
                'url': '',
                'type': 'GET',
                'success': function(result){
                    $('#tblAnnouncements').html($('#tblAnnouncements', result).html());
                    alert(data['message']['message']);
                }
            });
    });
});
</script>

<body>
    <div>
        <table style="min-width: 500px" border=2>
            <tr>
                <td width='250px'>
                    @include('layouts.adminside')
                </td>
                <td width='100%' style="vertical-align: top">
                    <div>
                        <table id='tblAnnouncements' width='100%'>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Sender
                                </th>
                                <th>
                                    Message
                                </th>
                                <th>
                                    Time
                                </th>
                            </tr>
                            @foreach ($announcements as $announcement)
                                <tr>
                                    <td>
                                        {{$announcement->id}}
                                    </td>
                                    <td>
                                        <a href="/user/{{$announcement->sender}}">{{$announcement->sender}}</a>
                                    </td>
                                    <td>
                                        {{$announcement->message}}
                                    </td>
                                    <td>
                                        {{$announcement->time}}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                        <table width='100%'>
                            <tr>
                                <td>
                                    <input type="text" name="announcement" id="inpAnnouncement" width='100%' style="width: 100%">
                                </td>
                                <td style="width: 1px">
                                    <button id='btnAnnounce'>Announce!</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>