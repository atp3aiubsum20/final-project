<!DOCTYPE html>
<html lang="en">
<head>
    <title>Admin</title>
</head>
<style>
    #tblStatus td{
        text-align: center;
    }
</style>

<body>
    <div>
        <table style="min-width: 500px" border=2>
            <tr>
                <td width='100%' style="vertical-align: top">
                    <div>
                        <table id='tblStatus' width='100%'>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Username
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Dob
                                </th>
                                <th>
                                    Type
                                </th>
                                <th>
                                    Joinng Date
                                </th>
                                <th>
                                    Ban Status
                                </th>
                            </tr>
                            @foreach ($userdata as $user)
                                <tr>
                                    <td>
                                        {{$user->user_id}}
                                    </td>
                                    <td>
                                        <a href="/user/{{$user->username}}">{{$user->username}}</a>
                                    </td>
                                    <td>
                                        {{$user->name}}
                                    </td>
                                    <td>
                                        {{$user->dob}}
                                    </td>
                                    <td>
                                        {{$user->type}}
                                    </td>
                                    <td>
                                        {{$user->joiningDate}}
                                    </td>
                                    <td>
                                        {{$user->ban}}
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>