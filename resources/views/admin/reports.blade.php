<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
</head>

<style>
    #tblReports td{
        text-align: center;
    }
</style>

<script>

</script>

<body>
    <div>
        <table style="min-width: 500px" border=2>
            <tr>
                <td width='250px'>
                    @include('layouts.adminside')
                </td>
                <td width='100%' style="vertical-align: top">
                    <div>
                        <table id='tblReports' width='100%'>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    PostID
                                </th>
                                <th>
                                    Poster
                                </th>
                                <th>
                                    Reporter
                                </th>
                                <th>
                                    Type
                                </th>
                                <th>
                                    Reason
                                </th>
                                <th>
                                    Time
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            @foreach ($reports as $report)
                                <tr>
                                    <td>
                                        {{$report->id}}
                                    </td>
                                    <td>
                                        <a href="/post/{{$report->postid}}">{{$report->postid}}</a>
                                    </td>
                                    <td>
                                        <a href="/user/{{$report->poster}}">{{$report->poster}}</a>
                                    </td>
                                    <td>
                                        <a href="/user/{{$report->reporter}}">{{$report->reporter}}</a>
                                    </td>
                                    <td>
                                        {{$report->type}}
                                    </td>
                                    <td>
                                        {{$report->reason}}
                                    </td>
                                    <td>
                                        {{$report->time}}
                                    </td>
                                    <td>
                                        <a href="/post/{{$report->postid}}">Review</a> | <a href='#' id='{{$report->id}}' class='ignoreReport'>Ignore</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>