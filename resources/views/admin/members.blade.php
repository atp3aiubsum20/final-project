<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
</head>

<style>
    #tblMembers td{
        text-align: center;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}',
        }
    });
    
    $('#btnSearch').click(function(e){
        var data={
            'search': $('#inpSearch').val(),
        }

        $.ajax({
            type: "get",
            data: data,
            success: function (result) {
                $('#tblMembers').html($('#tblMembers', result).html());
            }
        });
    });

    $('#inpSearch').on('keypress',function(e) {
        if(e.which == 13) {
            $('#btnSearch').trigger('click');
            $('#inpSearch').val('');
        }
    });
});
</script>

<body>
    <div>
        <table style="min-width: 500px" border=2>
            <tr>
                <td width='250px'>
                    @include('layouts.adminside')
                </td>
                <td width='100%' style="vertical-align: top">
                    <div>
                        <table width='100%'>
                            <tr>
                                <td>
                                    <input type="text" name="search" id="inpSearch" width='100%' style="width: 100%">
                                </td>
                                <td style="width: 1px">
                                    <button id='btnSearch'>Search</button>
                                </td>
                            </tr>
                        </table>
                        <table id='tblMembers' width='100%'>
                            <tr>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Username
                                </th>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    DoB
                                </th>
                                <th>
                                    Joining Date
                                </th>
                                <th>
                                    Action
                                </th>
                            </tr>
                            @foreach ($members as $member)
                                <tr style='@if ($member->ban == "1") color: red; @endif'>
                                    <td>
                                        {{$member->id}}
                                    </td>
                                    <td>
                                        <a href="/user/{{$member->username}}">{{$member->username}}</a>
                                    </td>
                                    <td>
                                        {{$member->fname}}
                                    </td>
                                    <td>
                                        {{$member->email}}
                                    </td>
                                    <td>
                                        {{$member->dob}}
                                    </td>
                                    <td>
                                        {{$member->joiningdate}}
                                    </td>
                                    <td>
                                        <a href="/user/{{$member->username}}">Review</a> | <a href="/admin/message/{{$member->id}}">Message</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>