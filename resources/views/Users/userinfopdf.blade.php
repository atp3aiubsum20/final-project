<!DOCTYPE html>
<html>
<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<head>
	<title>{{ Session::get('username') }}</title>
</head>
<body>
	
	<div class="container">
		<div class="row">
			<h3 class="col-md-2">Informations</h3>		
		</div>
		<div class="row">
			<span class="col-md-1"><b>Name</b></span>
			<span class="col-md-3">{{ $info->name}}	</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>Email</b></span>
			<span class="col-md-3">{{ $info->email }}</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>Username</b></span>
			<span class="col-md-3">{{ $info->username }}</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>Password</b></span>
			<span class="col-md-3">{{ $info->password }}</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>D.o.B.</b></span>
			<span class="col-md-3">{{ $info->dob }}		(YYYY:MM:DD)</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>School</b></span>
			<span class="col-md-3">{{ $info->school }}</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>College</b></span>
			<span class="col-md-3"><span class="col-md-3">{{ $info->college }}</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>University</b></span>
			<span class="col-md-3">{{ $info->university }}</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>Joined</b></span>
			<span class="col-md-3">{{ $info->joiningDate }}		(YYYY:MM:DD)</span>
		</div>
		
	</div>
		

</body>
</html>