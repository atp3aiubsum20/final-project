<html>
    @include('layouts.header');
    <head>
        <title>Search</title>
    </head>
    <body>
        @foreach ($members as $item)
            <div class="container bootstrap snippets bootdey">
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="well search-result">
                            <div class="row">
                                <a href="#">
                                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">
                                        <img class="img-responsive" src="/storage/Propics/{{ $item->displayimg}}" alt="Display Image"" alt="">
                                    </div>
                                    <div class="col-xs-6 col-sm-9 col-md-9 col-lg-10 title">
                                        <h3><b><a href="/user/{{$item->username}}">{{$item->username}}</a></b></h3>
                                        <p>{{$item->name}}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </body>
</html>