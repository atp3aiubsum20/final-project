<!DOCTYPE html>
<html>
@include('layouts.header');
<head>
	<title>{{ Session::get('username') }}</title>
</head>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}',
        }
    });

	var reqID = '{{$user->id}}';
	$('#unfriend').click(function (e) {
		$.post('/friends/fReqAction',{'reqId': reqID, action: 'unfriend'},function (res) {location.reload();});
	});
	$('#addFriend').click(function (e) {
		$.post('/friends/fReqAction',{'reqId': reqID, action: 'add'},function (res) {location.reload();});
	});
	$('#accept').click(function (e) {
		$.post('/friends/fReqAction',{'reqId': reqID, action: 'accept'},function (res) {location.reload();});
	});
	$('#reject').click(function (e) {
		$.post('/friends/fReqAction',{'reqId': reqID, action: 'reject'},function (res) {location.reload();});
	});
	$('#cancel').click(function (e) {
		$.post('/friends/fReqAction',{'reqId': reqID, action: 'cancel'},function (res) {location.reload();});
	});
	$('#ban').click(function (e) {
		$.post('/admin/banAction',{'target': reqID, action: 'ban'},function (res) {location.reload();});
	});
	$('#unban').click(function (e) {
		$.post('/admin/banAction',{'target': reqID, action: 'unban'},function (res) {location.reload();});
	});
});
</script>

<body>
	
	<div class="container">
		<div class="row">
			<h3 class="col-md-2">Informations</h3>
			@if (Auth::user()->type == 'admin')
				
				@if ($user->ban == '1')
					<span class="col-md-3" style="margin-top: 15px;">
						<button type="button" class="btn btn-warning" id='unban'>Unban</button></a>
					</span>
				@else
				<span class="col-md-3" style="margin-top: 15px;">
					<button type="button" class="btn btn-danger" id='ban'>Ban</button></a>
				</span>
				@endif

			@else
				@if(Session::get('username')==$info->username)
					<span class="col-md-3" style="margin-top: 15px;">
						<a href="{{ $info->username }}/edit"><button type="button" class="btn btn-success">Edit</button></a>
					</span>

				@elseif ($friendAction == 'add')
					<span class="col-md-3" style="margin-top: 15px;">
						<button type="button" class="btn btn-success" id="addFriend">Add Friend</button>
					</span>
				@elseif ($friendAction == 'unfriend')
					<span class="col-md-3" style="margin-top: 15px;">
						<button type="button" class="btn btn-danger" id="unfriend">Unfriend</button>
						<button type="button" class="btn btn-success" id="message" onclick="location.href = '/message/{{$user->id}}';">Message</button>
					</span>
				@elseif ($friendAction == 'accept')
					<span class="col-md-3" style="margin-top: 15px;">
						<button type="button" class="btn btn-success" id="accept">Accept</button>
						<button type="button" class="btn btn-danger" id="accept">Reject</button>
					</span>
				@elseif ($friendAction == 'cancel')
					<span class="col-md-3" style="margin-top: 15px;">
						<button type="button" class="btn btn-warning" id="cancel">Cancel Request</button>
					</span>
				@endif
			@endif
					
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="thumbnail">
					<img src="/storage/Propics/{{ $info->displayimg}}" alt="Display Image">
					<div class="caption">
	            		<h4><b>{{ $info->name}}</b></h4>
	          		</div>
				</div>
			</div>
		</div>
		<div class="row">
			<span class="col-md-1"><b>Name</b></span>
			<span class="col-md-3">{{ $info->name}}	</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>Email</b></span>
			<span class="col-md-3">{{ $info->email }}</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>Username</b></span>
			<span class="col-md-3">{{ $info->username }}</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>D.o.B.</b></span>
			<span class="col-md-3">{{ $info->dob }}		(YYYY:MM:DD)</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>School</b></span>
			<span class="col-md-3">{{ $info->school }}</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>College</b></span>
			<span class="col-md-3"><span class="col-md-3">{{ $info->college }}</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>University</b></span>
			<span class="col-md-3">{{ $info->university }}</span>
		</div>
		<div class="row">
			<span class="col-md-1"><b>Joined</b></span>
			<span class="col-md-3">{{ $info->joiningDate }}		(YYYY:MM:DD)</span>
		</div>
		@if ($friendAction == 'unfriend'||Auth::user()->type == 'admin'||Session::get('username')==$info->username)
		<div class="row">
			<a href="/user/{{$info->username}}/posts"> See posts </a>
		</div>
			
		@endif
		<div class="row">
			<form id="" action="/user/downloadData" method="POST">
			@csrf
			<button class="btn btn btn-primary col-md-3" type="submit">Download Data</button>
			</form>
		</div>
		
		
	</div>
		

</body>
</html>