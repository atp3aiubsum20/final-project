<!DOCTYPE html>
<html lang="en">
@include('layouts.header');
<head>
   <title>Edit Information</title>
</head>
<body>

<div class="container">
	<div class="row">
  		<h2>Edit Personal Info</h2>
  		<form class="form-horizontal" method="post" enctype="multipart/form-data">
  			@csrf
		  <div class="form-group">
	      		<label class="control-label col-sm-2" for="displayimg"><span style="color:red;font-size:18px ">*</span>Display Image:</label>
	      		<div class="col-md-3">
						<div class="thumbnail">
							<img src="/storage/Propics/{{ $info->displayimg}}" alt="Display Image">
							<div class="caption">
	            		<h4><b>{{ $info->name}}</b></h4>
	          		</div>
						 <div class="col-sm-6">
	        				<input id="propic" name="propic" type="file" class="file" data-browse-on-zone-click="true">
	      			</div>
				</div>
			</div>
	    	</div>
	    	<div class="form-group">
	      		<label class="control-label col-sm-2" for="name"><span style="color:red;font-size:18px ">*</span>Name:</label>
	      		<div class="col-sm-6">
	        		<input type="Text" class="form-control" id="name" placeholder="Enter Name" name="name" value="{{ $info->name}}" required>
	      		</div>
	    	</div>
	    	<div class="form-group">
	      		<label class="control-label col-sm-2" for="dob"><span style="color:red;font-size:18px ">*</span>Date of Birth:</label>
	      		<div class="col-sm-6">
	        		<input type="date" class="form-control" id="dob" name="dob" value="{{ Carbon\Carbon::parse($info->dob)->format('Y-m-d') }}" required>
	      		</div>
	    	</div>
		    <div class="form-group">
		      	<label class="control-label col-sm-2" for="school">school:</label>
		      	<div class="col-sm-6">
		        	<input type="Text" class="form-control" id="school" placeholder="Enter school" name="school" value="{{$info->school}}">
		      	</div>
		    </div>
		     <div class="form-group">
		      	<label class="control-label col-sm-2" for="college">College:</label>
		      	<div class="col-sm-6">
		        	<input type="Text" class="form-control" id="college" placeholder="Enter College" name="College" value="{{$info->college}}">
		      	</div>
		    </div>
	    	<div class="form-group">
	      		<label class="control-label col-sm-2" for="university">University:</label>
	      		<div class="col-sm-6">
	        		<input type="Text" class="form-control" id="university" placeholder="Enter University" name="university" value="{{$info->university}}">
	      		</div>
	    	</div>
		    <div class="form-group">        
		      	<div class="col-sm-offset-2 col-sm-10">
		        	<button type="submit" class="btn btn-success">Submit</button>
		      	</div>
		    </div>
  		</form>
	</div>
	<div class="row" style="color:red;">
		@foreach ($errors->all() as $error)
                <li>{{$error}}</li>
        @endforeach
	</div>

</body>
</html>