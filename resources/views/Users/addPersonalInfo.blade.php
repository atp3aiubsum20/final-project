<!DOCTYPE html>
<html lang="en">
<head>
  <title>Add Personal Info</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
	<div class="row">
  		<h2>Personal Info</h2>
  		<form class="form-horizontal" method="post" enctype="multipart/form-data">
  			@csrf
	    	<div class="form-group">
	      		<label class="control-label col-sm-2" for="name"><span style="color:red;font-size:18px ">*</span>Name:</label>
	      		<div class="col-sm-6">
	        		<input type="Text" class="form-control" id="name" placeholder="Enter Name" name="name" required>
	      		</div>
	    	</div>
	    	<div class="form-group">
	      		<label class="control-label col-sm-2" for="dob"><span style="color:red;font-size:18px ">*</span>Date of Birth:</label>
	      		<div class="col-sm-6">
	        		<input type="date" class="form-control" id="dob" name="dob" required>
	      		</div>
	    	</div>
		    <div class="form-group">
		      	<label class="control-label col-sm-2" for="school">school:</label>
		      	<div class="col-sm-6">
		        	<input type="Text" class="form-control" id="school" placeholder="Enter school" name="school">
		      	</div>
		    </div>
		     <div class="form-group">
		      	<label class="control-label col-sm-2" for="college">College:</label>
		      	<div class="col-sm-6">
		        	<input type="Text" class="form-control" id="college" placeholder="Enter College" name="College">
		      	</div>
		    </div>
	    	<div class="form-group">
	      		<label class="control-label col-sm-2" for="university">University:</label>
	      		<div class="col-sm-6">
	        		<input type="Text" class="form-control" id="university" placeholder="Enter University" name="university">
	      		</div>
	    	</div>
	    	<div class="form-group">
	      		<label class="control-label col-sm-2" for="propic">Profile Image:</label>
	      		<div class="col-sm-6">
	        		<input id="propic" name="propic" type="file" class="file" data-browse-on-zone-click="true">
	      		</div>
	    	</div>
		    <div class="form-group">        
		      	<div class="col-sm-offset-2 col-sm-10">
		        	<button type="submit" class="btn btn-default">Submit</button>
		      	</div>
		    </div>
  		</form>
	</div>
	<div class="row" style="color:red;">
		@foreach ($errors->all() as $error)
                <li>{{$error}}</li>
        @endforeach
	</div>

</body>
</html>