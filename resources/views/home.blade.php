@extends('layouts.app')
@include('layouts.header');
<head>
    <title>Home</title>
</head>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    welcome {{ session('username') }}
                    <div class="form-group">        
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success"><a href="/createpost">Create post</a></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
