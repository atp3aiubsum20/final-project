<!DOCTYPE html>
<html lang="en">
@include('layouts.header');
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Friend List</title>
</head>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}',
            }
        });
    
        $('.btnAccept').click(function(e){
            $.post(location.href+'/fReqAction',{reqId: e.currentTarget.value, action: 'accept'}, function(result){
                $('#tblReq').html($('#tblReq', result).html());
                $('#tblList').html($('#tblList',result).html());
            });
        });
    
        $('.btnReject').click(function(e){
            $.post(location.href+'/fReqAction',{reqId: e.currentTarget.value, action: 'reject'}, function(result){
                $('#tblReq').html($('#tblReq',result).html());
                
            });
        });
    
        $('.btnMessage').click(function(e){
            location.href = "/message/"+e.currentTarget.value;
        });
    });
</script>

<body>
    <div style="text-align: center;">
        Friend Requests<br>
        <div style="display: inline-block; text-align: left; border-style: solid; padding: 10px; width: fit-content;">
            <table id='tblReq' style="width: 500px;">
                @if (isset($frndReqs) && count($frndReqs) > 0)
                    @foreach ($frndReqs as $req)
                    <tr>
                        <td width="100%">
                            <a href="{{$req->username}}">{{$req->username}}</a>
                        </td>
                        <td style="width: fit-content;">
                            <button value="{{$req->personid}}" class="btnAccept">Accept</button>
                        </td>
                        <td style="width: fit-content;">
                            <button value="{{$req->personid}}" class="btnReject" style="background-color: rgba(255, 0, 0, 0.2);">Reject</button>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td style="text-align: center;">
                            No Friend Requests pending
                        </td>
                    </tr>
                @endif
            </table>
        </div>
        <br>Friend List<br>        
        <div style="display: inline-block; text-align: left; border-style: solid; padding: 10px; width: fit-content;">
            <table id='tblList' style="width: 500px;">
                @if (isset($frndList) && count($frndList) > 0)
                    @foreach ($frndList as $frnd)
                    <tr>
                        <td width="100%">
                            <a href="/user/{{$frnd->username}}">{{$frnd->username}}</a>
                        </td>
                        <td style="width: fit-content;">
                            <button value="{{$frnd->friendid}}" class="btnMessage">Message</button>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td style="text-align: center;">
                            You have not made any friends yet!
                        </td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
    <div style="text-align: center; margin-top: 20px;">
        <a href="/friends/find" style="text-align: center;">Find People</a>    
    </div>
</body>
</html>