<!DOCTYPE html>
<html lang="en">
@include('layouts.header');
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Messaging</title>
</head>

<style>
    div.left {
        /* height:fit-content; */
        /* border: 1px red solid; */
        width: 80%;
        position: relative;
        float: left; 
        align-items: left;
        text-align: left;

        margin: 5px 2px 5px 2px;
    }

    div.right {
        /* height:fit-content; */
        /* border: 1px green solid; */
        width: 80%;
        position: relative;
        float: right;
        text-align: right;
        background-color: palegreen;

        margin: 5px 2px 5px 2px;
    }

    #tblList tr{
        white-space: nowrap; 
        width: 150px;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    
    #tblList tr.listUnseen{
        font-weight: bold;
    }

    #tblList tr.currActive{
        background-color: lightgrey;
    }
    
    #tblList tr:hover{
        cursor:pointer;
        text-decoration:underline;
    }

    hr { 
        display: block;
        margin-top: -0.5em;
        margin-bottom: -0.5em;
        margin-left: auto;
        margin-right: auto;
        border-style: solid;
        border-width: 1px;
    } 
    table#tblUnseen tr:last-child { 
        border-bottom: 1px solid black; 
    }

    .unseen{
        font-weight: bold;
        width: 100%;
        color: darkgreen;
    }
    .seen{
        width: 100%;
        color:black;
    }

    .unseen .fadedmsg{
        padding-left: 10px;
        padding-right: 10px;
        font-weight: normal;
        color:darkgreen;
    }
    .seen .fadedmsg{
        padding-left: 10px;
        padding-right: 10px;
        color:grey;
    }

</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>
$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}',
        }
    });

    $('#tblList').on("click", "tr", (function(e){

        /* console.log("ID: ");
        console.log(e.currentTarget.id); */
        $(location).prop('href','/message/'+e.currentTarget.id+'');
    }));

    //Send Message
    $('#btnMsgPost').click(function(e){
        if ($("#inpMsg").val() != ""){
            var data={
                'receiverid': '{{$user['to']}}',
                'description': $('#inpMsg').val(),
            }
            $.ajax({
                'url': '',
                'data': data,
                'type': 'POST',
            });
            $("#inpMsg").val('');
        }
    });

    $('#inpMsg').on('keypress',function(e) {
        if(e.which == 13) {
            $('#btnMsgPost').trigger('click');
            $('#inpMsg').val('');
        }
    });

    // Pusher

    var pusher2 = new Pusher('e2d10f9ae5c8245db0b4', {
        cluster : 'ap1',
    });

    var channel1 = pusher2.subscribe('message');
    channel1.bind('App\\Events\\MessageSent', function(data) {
        $.ajax({
                'url': '',
                'type': 'GET',
                'success': function(result){
                    $('#tblMsgShow').html($('#tblMsgShow', result).html());
                    $('#tblList').html($('#tblList', result).html());
                }
            });
    });
    
    var pusher2 = new Pusher('e2d10f9ae5c8245db0b4', {
        cluster : 'ap1',
    });

    var channel2 = pusher2.subscribe('announcement');
    channel2.bind('App\\Events\\AnnouncementSent', function(data) {
        $.ajax({
                'url': '',
                'type': 'GET',
                'success': function(result){
                    alert(data['message']['message']);
                }
            });
    });
})

    

</script>

<body id="contentBody">
    <div style="text-align: center;">
        <h1><span id="spnRecpt"></span></h1><br>
        <div style="display: inline-block; text-align: left; width: fit-content;">
            <table border="1px" style="border-color: yellowgreen;">
                <tr>
                    <td style="vertical-align: top;">
                        <table width="250px">
                            <tr>
                                <td>
                                    <table width="100%" id="tblList">
                                        @foreach ($friends as $friend)
                                            <tr id='{{$friend->friendid}}' class="@if($friend->unseen > 0) unseen @else seen @endif">
                                                <td style="width: 1px; padding-left: 10px">
                                                    {{$friend->username}}
                                                </td>
                                                <td class='fadedmsg'>
                                                    @if (isset($friend->lastmsg->description))
                                                        {{$friend->lastmsg->description}}
                                                    @endif
                                                </td>
                                                @if ($friend->unseen > 0)
                                                    <td style="width: 1px; padding-right: 10px">
                                                        {{$friend->unseen}}
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table width="350px">
                            <tr>
                                <td>
                                    <div id="tblMsgShow" style="width: 100%; overflow: hidden">                                     
                                        @foreach ($messages as $message)
                                        @if ($message['senderid'] == $user['from'])
                                        <div class="right">
                                            {{$message['description']}}
                                        </div>
                                        @elseif ($message['senderid'] == $user['to'])
                                        <div class="left">
                                            {{$message['description']}}
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width='100%'>
                                        <tr>
                                            <td style="width: 100%;">
                                                <input type="text" name="msg" id="inpMsg" style="width: 100%;">
                                            </td>
                                            <td style="width: min-content;">
                                                <button id="btnMsgPost">Send</button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>