@section('adminside')
<style>
    a{
        color: inherit;
    }
    a.disabled{
        pointer-events: none;
        cursor: default;
        color: inherit;
        text-decoration: none;
    }
    li {
        padding-top: 5px;
        padding-bottom: 5px;
    }

</style>

<div>
    <ul style="list-style-type:none; padding: 20px 10px 20px 10px;">
        <li>
            <a href="/admin/reports" @if (Request::path() == 'admin/reports') class='disabled' @endif>Reports</a>
        </li>
        <li>
            <a href="/admin/members" @if (Request::path() == 'admin/members') class='disabled' @endif>Members</a>
        </li>
        <li>
            <a href="/admin/announcements" @if (Request::path() == 'admin/announcements') class='disabled' @endif>Announcements</a>
        </li>
        <li>
            <a href="/admin/status" @if (Request::path() == 'admin/status') class='disabled' @endif>Stats</a>
        </li>
        <li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
            <a href="#" onclick="document.getElementById('logout-form').submit()">Logout</a>
        </li>
    </ul>
</div>
@show