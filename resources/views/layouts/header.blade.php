<!DOCTYPE html>
<html>
<head>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  	<script>
      	$(document).ready(function(){
			$('#btnSearch').click(function(){
				var search = $('#searchInput').val();
				$.ajax({
					'url': '/usersearch',
					'data': {'search': search},
					'type': 'GET',
					'async': false,
            	});
			});
      	});
  	</script>
</head>

<body style="margin: 0;">
   @section('header')
	<div class="row" style="background-color:#204051"> 
        <div class="col-md-3" ><a href="/home" class="logolink"><img src="/images/logo.png" height="77" width="265" style="margin-left:20px"></a></div>
        <form action="/usersearch" method="GET" >
			<div class="col-md-4"><input id="searchInput" class="form-control" type="text" name="search" class="search" style="margin-top: 20px;"></div>
			<div class="col-md-1"><button id="btnSearch" type="submit" class="btn btn-success" style="margin-top: 20px;">Search</button></div>
		</form>
		
        <div class="col-md-1" style="margin-top: 20px;"><a style="color: white; text-decoration: none;font-size:20px;" href="/user/{{ Session::get('username') }}">{{ Session::get('username') }}</a></div>
        <div class="col-md-1" style="margin-top: 20px;"><a href="/friends"><img class="bell" src="/images/friends_h.png" width="30px" height="30px" ></a></div>
        <div class="col-md-1" style="margin-top: 20px;"><a href="/message"><img class="envelope" src="/images/envelope.png" width="40px" height="25px"></a></div>
        <div class="col-md-1" style="margin-top: 20px;color:white;"><a style="color: white; text-decoration: none;font-size:20px;" class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form></a></div>	
    </div>
    @show
</body>
</html>