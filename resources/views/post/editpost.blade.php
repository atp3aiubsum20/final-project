<!DOCTYPE html>
<html lang="en">
@include('layouts.header');
<head>
   <title>Create post</title>
   <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
   <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
   <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            
            <div class="col-md-8 col-md-offset-2">
                
                <h1>Create post</h1>
                
                <form action="" method="POST" enctype="multipart/form-data">
                  @csrf  
						<div class="form-group">
							<label for="description">Description</label>
							<textarea rows="5" class="form-control" name="description" >{{ $post->description}}</textarea>
						</div>
						<div class="form-group">
							<img src="/storage/PostImages/{{ $post->postimg}}" alt="Post Image">
						</div>  
						<div>
							<button type="submit" class="btn btn-primary" >Confirm Edit</button>	
						</div>
                </form>
					 <div class="row" style="color:red;">
						@foreach ($errors->all() as $error)
									<li>{{$error}}</li>
						@endforeach
					</div>
            </div>
            
        </div>
    </div>
	 

</body>
</html>