<!DOCTYPE html>
<html lang="en">
@include('layouts.header');
<head>
<title>Post</title>
</head>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>

$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}',
        }
    });

    $(document).on('click','button.btnComment',function(e){
        console.log(e.target.id);
        $.ajax({
        type: "POST",
        data: {
            'description': $('#inp'+e.target.id).val(),
        },
        url: "/post/"+e.target.id,
        success: function (result) {       
                $('#post'+e.target.id).html($('#post'+e.target.id, result).html());
            }
        });
    });
});

</script>

    <body>
        <div>
            <div class="container postShow">
                <div class="row">
                    
                    <div class="col-md-10 col-md-offset-2 container">
                        <div class= "row">
                            <div class="col-md-8">
                                <h3><a href="/user/{{$poster->username}}"><b>{{$poster->username}}</b></a></h3>   
                            </div>
                            @if($poster->username == Session::get('username'))
                            <div class="col-md-2">
                                    <a href="/post/{{$post->id}}/edit"><button type="button" class="btn btn-success">Edit</button></a>
                            </div>
                            @endif
                        </div>
                        
                        
                        @if(isset($post->postimg))
                        <br>
                        <div class="row">
                            <div>
                                    <img src="/storage/postImages/{{ $post->postimg}}" alt="Attached post Image" style="max-width: 700px">
                            </div>
                            @endif
                        </div>
                        
                        <br>
                        
                        <div class="row">
                            {{$post->description}}
                        </div> 
    
                        <br>
    
                        <div class="row" id='post{{$post->id}}'>
                            @foreach ($post->comments as $comment)
                                <div class="col-sm">
                                    {{$comment->commenter}}
                                </div> 
                                <div class="col-sm-8">
                                    {{$comment->description}}
                                </div>
                                <div>
                                    &nbsp;
                                </div>
                            @endforeach
                        </div> 
                    </div>
                        <div class="row">
                        <input type="text" class="form-control" id="inp{{$post->id}}">
                        <button class='btnComment' id="{{$post->id}}">Comment</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
