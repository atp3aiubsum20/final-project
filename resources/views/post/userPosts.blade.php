<!DOCTYPE html>
<html lang="en">
    @include('layouts.header');
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function () {
    
    var postids = {!!$postids!!};

    postids.forEach(postid => {
        $.ajax({
        type: "GET",
        url: "/post/"+postid,
        success: function (result) {
                //console.log($('.postShow', result).html());
                $('#mainContainer').append($(result).find('.postShow'));
            }
        });
    });

    $('.btnComment').click(function(e){
        console.log(e.target.id);
        $.ajax({
        type: "POST",
        data: {
            'description': $('#inp'+e.target.id).val(),
        },
        url: "/post/"+e.target.id,
        success: function (result) {       
                $('#post'+e.target.id).html($('#post'+e.target.id, result).html());
            }
        });
    });
});


</script>
<body>
    <div id="mainContainer" class="">
        
    </div>
</body>
</html>